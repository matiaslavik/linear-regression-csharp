﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinearRegression
{
    struct Sample
    {
        public double x;
        public double y;

        public Sample(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }

    struct LineModel
    {
        public double intercept;
        public double slope;
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Sample> samples1 = new List<Sample>() {  new Sample(1.0f, 3.0f), new Sample(2.0f, 2.0f), new Sample(3.0f, 3.0f),
                                            new Sample(4.0f, 5.0f), new Sample(5.0f, 4.0f), new Sample(6.0f, 6.0f),
                                            new Sample(7.0f, 7.0f), new Sample(8.0f, 8.0f), new Sample(9.0f, 8.0f)};

            List<Sample> samples2 = new List<Sample>() {  new Sample(1.0f, 1.0f), new Sample(2.0f, 1.5f), new Sample(3.0f, 2.0f),
                                            new Sample(4.0f, 2.5f), new Sample(5.0f, 3.0f), new Sample(6.0f, 3.5f),
                                            new Sample(7.0f, 4.0f), new Sample(8.0f, 4.5f), new Sample(9.0f, 5.0f)};

            RunExample(samples1);
            RunExample(samples2);
        }

        private static void RunExample(List<Sample> samples)
        {
            List<double> xSamples = samples.Select(s => { return s.x; }).ToList();
            double stx = GetStandardDeviation(xSamples);
            double pearsonCorrelation = GetPearsonCorrelation(samples);

            // Fit the regression line (standard method)
            LineModel lineA = FitRegressionLine_A(samples);
            // Fit the regression line (using the Pearson Correlation Coefficient)
            LineModel lineB = FitRegressionLine_B(samples);

            Console.WriteLine("Standard deviation X: " + stx);
            Console.WriteLine("Pearson Correlation Coefficient: " + pearsonCorrelation);
            Console.WriteLine("Line A intercept: " + lineA.intercept);
            Console.WriteLine("Line A slope: " + lineA.slope);
            Console.WriteLine("Line B intercept: " + lineB.intercept);
            Console.WriteLine("Line B slope: " + lineB.slope);
        }

        // Calculate the slope and intercept of the line.
        public static LineModel FitRegressionLine_A(List<Sample> samples)
        {
            LineModel result;

            List<double> xSamples = samples.Select(s => { return s.x; }).ToList();
            List<double> ySamples = samples.Select(s => { return s.y; }).ToList();
            double xMean = GetMean(xSamples);
            double yMean = GetMean(ySamples);

            double numerator = 0.0f;
            foreach (Sample sample in samples)
                numerator += (sample.x - xMean) * (sample.y - yMean);
            double denominator = 0.0f;
            foreach (Sample sample in samples)
                denominator += (sample.x - xMean) * (sample.x - xMean);
            result.slope = numerator / denominator;

            result.intercept = yMean - result.slope * xMean;

            return result;
        }

        // Calculate the slope and intercept of the line, using the Pearson Correlation Coefficient
        public static LineModel FitRegressionLine_B(List<Sample> samples)
        {
            LineModel result;

            List<double> xSamples = samples.Select(s => { return s.x; }).ToList();
            List<double> ySamples = samples.Select(s => { return s.y; }).ToList();
            double xMean = GetMean(xSamples);
            double yMean = GetMean(ySamples);
            double stx = GetStandardDeviation(xSamples);
            double sty = GetStandardDeviation(ySamples);
            double pearsonCorrelation = GetPearsonCorrelation(samples);

            result.slope = pearsonCorrelation * sty / stx;

            result.intercept = yMean - result.slope * xMean;

            return result;
        }

        // Calculate the Pearson Correlation Coefficient
        // This value indicates how linearly related the two variables (X and Y) are.
        // The value is between -1 and +1. 0 indicates no linear correlation, and +1 and -1 indicate a positive/negative linear correlation.
        private static double GetPearsonCorrelation(List<Sample> samples)
        {
            List<double> xSamples = samples.Select(s => { return s.x; }).ToList();
            List<double> ySamples = samples.Select(s => { return s.y; }).ToList();
            double xMean = GetMean(xSamples);
            double yMean = GetMean(ySamples);
            double stx = GetStandardDeviation(xSamples);
            double sty = GetStandardDeviation(ySamples);

            double diffSum = 0.0f;
            foreach (Sample sample in samples)
                diffSum += (sample.x - xMean) * (sample.y - yMean);
            return diffSum / (samples.Count * stx * sty);
        }

        // Calculate the standard deviation (amount of variation).
        private static double GetStandardDeviation(List<double> samples)
        {
            double mean = GetMean(samples);
            double variance = 0.0f;
            foreach (double val in samples)
                variance += (val - mean) * (val - mean);
            return Math.Sqrt(variance / samples.Count);
        }

        private static double GetMean(List<double> samples)
        {
            double total = 0.0f;
            foreach (double val in samples)
                total += val;
            return total / samples.Count;
        }
    }
}

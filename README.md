Example of linear regression, implemented in C#.

**NOTE:** This is highly inefficient, and should not be used as it is.
The means and standard deviations are recalculated several times.
I did it like this on purpose, to make the example more readable/understandable.